function updateAll() {
	verifySession();
	getInfo();
}

function updateInfo() {
    var xhr = new XMLHttpRequest();
    var firstname = document.getElementById('firstname').value;
    var lastname = document.getElementById('lastname').value;
    var email = document.getElementById('email').value;
    var persinfoJSON = {
        "firstName": firstname,
        "lastName": lastname,
        "email": email
    };
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            let data = xhr.responseText;
            if(!data){
                //not logged in, must redirect
                //no idea how this could happen but just in case
				//falsy empty string...
				window.location.href = "login.html";
            } else {
                alert(data);
                getInfo();
            }
        }
    };
    xhr.open('POST', '/Project1Maven/UpdateInfo');
    xhr.send(JSON.stringify(persinfoJSON));
	return false;
}

function getInfo() {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            let data = xhr.responseText;
            if (!data) {
				//not logged in, must redirect
                //no idea how this could happen but just in case
				//falsy empty string...
				window.location.href = "login.html";
			} else {
                let upi = JSON.parse(data);
				document.getElementById("upiheader").innerHTML = upi.firstName + " " + upi.lastName + " " + upi.email;
				document.getElementById('firstname').value = upi.firstName;
    			document.getElementById('lastname').value = upi.lastName;
    			document.getElementById('email').value = upi.email;
			}
        }
    };
    xhr.open('POST', '/Project1Maven/GetInfo');
    xhr.send();
}


function verifySession() {
    var xhrUser = new XMLHttpRequest();
	xhrUser.onreadystatechange = function () {
		if (xhrUser.readyState == XMLHttpRequest.DONE) {
			let data = xhrUser.responseText;
			if (!data) {
				//not logged in, must redirect
				//falsy empty string...
				window.location.href = "login.html";
			} else {
				let upr = JSON.parse(data);
				if(upr.role.role != "EMPLOYEE") {
					window.location.href = "login.html";
				} else {
					document.getElementById("unheader").innerHTML = upr.username;
				}
			}
		}
	};
	xhrUser.open('POST', '/Project1Maven/CheckSession');
	xhrUser.send();
}