# Project 1 - Expense Reimbursement System

## Project Description

The Expense Reimbursement System webapp manages the processing of reimbursing employees for expenses. Employees in the company can register, log in, and submit requests for reimbursement including a description, amount, and type. Managers can log in and view all reimbursement requests and optionally filter by employee. They can deny or approve requests, and both managers and employees can view pending and resolved requests to which they have access.

## Technologies Used

* Java
* Javascript
* HTML
* CSS
* SQL
* AJAX
* Bootstrap
* RDS
* Tomcat
* Git
* Maven

## Features

* All users can log in and employees can register
* Employees can submit and view requests
* Managers can view all requests or filter by employee
* Managers can approve or deny requests
* Employees can view and change their personal information


## Getting Started
   
1) Clone the repository using the provided gitlab clone command: `git clone https://gitlab.com/ajohnsonRevature/project1-expense-reimbursement-system.git`
2) Import the project into Eclipse EE as a Maven Project.
3) Install dependencies using Maven.
4) Email alex.johnson@revature.net to start the database instance on AWS. (Prior to 2022 the database should be perpetually running.)
5) Stop any services currently using port 8080.
6) Run the application as "Run on Server".
7) Navigate to http://localhost:8080/Project1Maven/login.html in the browser.

## Usage

There is currently no way to register a manager account, so the in-built manager accounts are AlexM with password 12345 and GregM with password 54321. Otherwise the website should be intuitive and easy to use. 
