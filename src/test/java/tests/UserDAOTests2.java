package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import database.DB;
import exceptions.NonexistingUserException;
import users.User;
import users.UserOracleDAO;
import users.UserPersInfo;
import users.UserRole;
import users.UserUPR;

public class UserDAOTests2 {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		DB.closeConn();
	}

	@Before
	public void setUp() throws Exception {
		DB.truncateAll();
	}

	@After
	public void tearDown() throws Exception {
		DB.truncateAll();
	}

	@Test
	public void testInsertUser() {
		User u = new User(0, new UserUPR("AlexE", "12345", new UserRole("EMPLOYEE")), new UserPersInfo("Alex", "Employee", "alexe@gmail.com"));
		assertTrue(UserOracleDAO.getInstance().insertUser(u));
		assertFalse(UserOracleDAO.getInstance().insertUser(u));
	}
	
	@Test
	public void testGetUserByUsername() {
		User u = new User(0, new UserUPR("AlexE", "12345", new UserRole("EMPLOYEE")), new UserPersInfo("Alex", "Employee", "alexe@gmail.com"));
		try {
			assertNull(UserOracleDAO.getInstance().getUserByUsername("AlexE"));
		} catch (NonexistingUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(UserOracleDAO.getInstance().insertUser(u));
		User u2 = null;
		try {
			u2 = UserOracleDAO.getInstance().getUserByUsername("AlexE");
		} catch (NonexistingUserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertNotNull(u2);
		assertEquals("12345", u2.getUPR().getPassword());
		assertEquals("Alex", u2.getUPI().getFirstName());
		assertEquals("Employee", u2.getUPI().getLastName());
		assertEquals("alexe@gmail.com", u2.getUPI().getEmail());
		assertEquals("EMPLOYEE", u2.getUPR().toString());
	}

}
