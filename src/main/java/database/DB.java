package database;

import java.sql.*;


public class DB {

	static final String user = "admin";
	static final String password = "12345678";
	static final String db_url = "jdbc:oracle:thin:@database-1.cmf2toyaz9wt.us-east-2.rds.amazonaws.com:1521:orcl";
	
	private static DB inst = null;

	public Connection conn;
	public Statement stmt;
	
	private DB() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(db_url, user, password);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static DB getInstance() {
		if(inst == null) {
			inst = new DB();
		}
		return inst;
	}
	
	public static void closeConn() {
		try {
			getInstance().conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void truncateAll() {
		String[] commands = {"BEGIN\r\n"
				+ "  FOR c IN (SELECT table_name, constraint_name FROM user_constraints WHERE constraint_type = 'R')\r\n"
				+ "  LOOP\r\n"
				+ "    EXECUTE IMMEDIATE ('alter table ' || c.table_name || ' disable constraint ' || c.constraint_name);\r\n"
				+ "  END LOOP;\r\n"
				+ "  EXECUTE IMMEDIATE('truncate table ers_reimbursement');\r\n"
				+ " EXECUTE IMMEDIATE('truncate table ers_users');\r\n"
				+ "  FOR c IN (SELECT table_name, constraint_name FROM user_constraints WHERE constraint_type = 'R')\r\n"
				+ "  LOOP\r\n"
				+ "    EXECUTE IMMEDIATE ('alter table ' || c.table_name || ' enable constraint ' || c.constraint_name);\r\n"
				+ "  END LOOP;\r\n"
				+ "END;"};
		update(commands);
	}

	public static ResultSet query(String command) {
		ResultSet rs = null;
		try {
			getInstance().stmt = getInstance().conn.createStatement();
			rs = getInstance().stmt.executeQuery(command);
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rs;
	}

	public static boolean update(String command) {
		String[] commands = {command};
		return update(commands);
	}
	
	public static boolean update(String[] commands) {
		try {
			getInstance().stmt = getInstance().conn.createStatement();
			for (String c : commands) {
				getInstance().stmt.execute(c);
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
