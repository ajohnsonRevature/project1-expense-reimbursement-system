package reimbursements;

import java.io.Serializable;

public class ReimbursementStatus implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReimbursementStatus(String status) {
		super();
		this.status = status.toUpperCase();
	}

	private String status;
	
	@Override
	public String toString() {
		return status.toUpperCase();
	}
}
