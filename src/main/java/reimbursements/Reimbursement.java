package reimbursements;

import java.io.Serializable;

public class Reimbursement implements Serializable {
	
	private int id;
	private double amount;
	private int userID;
	private String description;
	private ReimbursementType type;
	private ReimbursementStatus status;
	
	public Reimbursement(int id, double amount, int userID, String description, ReimbursementType type,
			ReimbursementStatus status) {
		super();
		this.id = id;
		this.amount = amount;
		this.userID = userID;
		this.description = description;
		this.type = type;
		this.status = status;
	}
	
	public Reimbursement(String type, double amount, String description, int userID) {
		this(0, amount, userID, description, new ReimbursementType(type.toUpperCase()), new ReimbursementStatus("APPLIED"));
	}
	
	public Reimbursement() {
		this.status = new ReimbursementStatus("PENDING");
		this.userID = 0;
	}
	
	public static boolean validRequest(String type, String amountString, String description) {
		double amount;
		if(type == null || amountString == null || description == null) {
			return false;
		}
		try {
			amount = Double.parseDouble(amountString);
		} catch(NullPointerException | NumberFormatException e) {
			return false;
		}
		return amount > 0;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public double getAmount() {
		return amount;
	}



	public void setAmount(double amount) {
		this.amount = amount;
	}



	public int getUserID() {
		return userID;
	}



	public void setUserID(int userID) {
		this.userID = userID;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public ReimbursementType getType() {
		return type;
	}



	public void setType(ReimbursementType type) {
		this.type = type;
	}



	public ReimbursementStatus getStatus() {
		return status;
	}



	public void setStatus(ReimbursementStatus status) {
		this.status = status;
	}
}
