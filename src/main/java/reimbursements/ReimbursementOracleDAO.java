package reimbursements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



import database.DB;
import users.User;
import users.UserOracleDAO;
import users.UserRole;


public class ReimbursementOracleDAO implements ReimbursementDAO{
	
	private static ReimbursementOracleDAO instance = null;
	private PreparedStatement ps;
	
	private ReimbursementOracleDAO() {
		
	}
	
	public static ReimbursementOracleDAO getInstance() {
		if(instance == null) {
			instance = new ReimbursementOracleDAO();
		}
		return instance;
	}
	

	@Override
	public List<Reimbursement> getAllReimbursements() {
		//this is a lazy and kind of bad way to do this...
		List<Reimbursement> reimbs = new ArrayList<Reimbursement>();
		List<User> users = UserOracleDAO.getInstance().getAllUsersByRole(new UserRole("EMPLOYEE"));
		for(User u : users) {
			reimbs.addAll(ReimbursementOracleDAO.getInstance().getAllReimbursementsByUser(u));
		}
		return reimbs;
	}

	@Override
	public boolean insertReimbursement(Reimbursement reimbToInsert) {
		try {
			String command = "INSERT INTO ers_reimbursement VALUES (ers_reimb_seq.NEXTVAL, ?, ?, ?, ?, ?)";
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setString(1, reimbToInsert.getStatus().toString());
			ps.setString(2, reimbToInsert.getType().toString());
			ps.setString(3, reimbToInsert.getDescription());
			ps.setInt(4, reimbToInsert.getUserID());
			ps.setFloat(5, (float) reimbToInsert.getAmount());
			ps.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateReimbursement(Reimbursement reimbToUpdate) {
		//only updating the one thing can can change for now...
		String command = "UPDATE ers_reimbursement SET reimb_status = ? WHERE reimb_id = ?";
		try {
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setString(1, reimbToUpdate.getStatus().toString().toUpperCase());
			ps.setInt(2, reimbToUpdate.getId());
			return ps.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Reimbursement> getAllReimbursementsByUser(User u) {
		//really just looks for reimbursements associated to this user's ID
		List<Reimbursement> reimbs = new ArrayList<Reimbursement>();
		String command = "SELECT r.reimb_id, r.reimb_status, r.reimb_type, r.reimb_description, r.reimb_amount FROM (ers_reimbursement r INNER JOIN ers_users u ON r.reimb_user_id = u.ers_user_id) WHERE u.ers_user_id = ?";
		try {
			PreparedStatement ps = DB.getInstance().conn.prepareStatement(command);
			ps.setInt(1, u.getId());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int reimbID = rs.getInt(1);
				ReimbursementStatus reimbStatus = new ReimbursementStatus(rs.getString(2));
				ReimbursementType reimbType = new ReimbursementType(rs.getString(3));
				String reimbDescription = rs.getString(4);
				double reimbAmount = rs.getFloat(5);
				Reimbursement r = new Reimbursement(reimbID, reimbAmount, u.getId(), reimbDescription, reimbType, reimbStatus);
				reimbs.add(r);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reimbs;
	}

	@Override
	public Reimbursement getReimbursementByID(int id) {
		Reimbursement r = null;
		String command = "SELECT * FROM ers_reimbursement WHERE reimb_id = ?";
		try {
			PreparedStatement ps = DB.getInstance().conn.prepareStatement(command);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				ReimbursementStatus reimbStatus = new ReimbursementStatus(rs.getString(2));
				ReimbursementType reimbType = new ReimbursementType(rs.getString(3));
				String reimbDescription = rs.getString(4);
				int userid = rs.getInt(5);
				double reimbAmount = rs.getFloat(6);
				r = new Reimbursement(id, reimbAmount, userid, reimbDescription, reimbType, reimbStatus);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return r;
	}
}
