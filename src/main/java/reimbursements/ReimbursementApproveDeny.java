package reimbursements;

import java.io.Serializable;

public class ReimbursementApproveDeny implements Serializable {
	
	private int rid;
	private String mode;
	
	public ReimbursementApproveDeny() {
		
	}
	
	public boolean execute() {
		Reimbursement r = ReimbursementOracleDAO.getInstance().getReimbursementByID(rid);
		if(r == null) {
			return false;
		} else {
			if(mode.equalsIgnoreCase("approve")) {
				r.setStatus(new ReimbursementStatus("APPROVED"));
			} else if (mode.equalsIgnoreCase("deny")) {
				r.setStatus(new ReimbursementStatus("DENIED"));
			} else {
				return false;
			}
			return ReimbursementOracleDAO.getInstance().updateReimbursement(r);
		}
	}
	
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}

}
