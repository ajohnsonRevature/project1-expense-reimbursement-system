package users;

import java.io.Serializable;

public class UserRole implements Serializable {
	
	
	private String role;
	
	public UserRole() {
		
	}
	
	public UserRole(String role) {
		this.role = role.toUpperCase();
	}
	
	@Override
	public String toString() {
		return role.toUpperCase();
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
