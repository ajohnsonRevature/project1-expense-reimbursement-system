package users;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import database.DB;
import exceptions.NonexistingUserException;
import reimbursements.Reimbursement;
import reimbursements.ReimbursementStatus;
import reimbursements.ReimbursementType;

public class UserOracleDAO implements UserDAO {

	private static UserOracleDAO instance = null;
	private PreparedStatement ps;

	private UserOracleDAO() {

	}

	public static UserDAO getInstance() {
		if (instance == null) {
			instance = new UserOracleDAO();
		}
		return instance;
	}

	@Override
	public List<User> getAllUsersByRole(UserRole role) {
		List<User> users = new ArrayList<User>();
		String command = "SELECT * FROM ers_users WHERE ers_user_role = ?";
		try {
			PreparedStatement ps = DB.getInstance().conn.prepareStatement(command);
			ps.setString(1, role.toString().toUpperCase());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				int userID = rs.getInt(1);
				String username = rs.getString(2);
				String password = rs.getString(3);
				String firstName = rs.getString(4);
				String lastName = rs.getString(5);
				String email = rs.getString(6);
				UserPersInfo upi = new UserPersInfo(firstName, lastName, email);
				User u = new User(userID, new UserUPR(username, password, role), upi);
				users.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public boolean insertUser(User u) {
		try {
			String command = "INSERT INTO ers_users VALUES (ers_users_seq.NEXTVAL, ?, ?, ?, ?, ?, ?)";
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setString(1, u.getUPR().getUsername());
			ps.setString(2, u.getUPR().getPassword());
			ps.setString(3, u.getUPI().getFirstName());
			ps.setString(4, u.getUPI().getLastName());
			ps.setString(5, u.getUPI().getEmail());
			ps.setString(6, u.getUPR().getRole().toString());
			ps.execute();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean updateUser(User userToUpdate) {
		String command = "UPDATE ers_users SET user_first_name = ?, user_last_name = ?, user_email = ? WHERE ers_user_id = ?";
		try {
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setString(1, userToUpdate.getUPI().getFirstName());
			ps.setString(2, userToUpdate.getUPI().getLastName());
			ps.setString(3, userToUpdate.getUPI().getEmail());
			ps.setInt(4, userToUpdate.getId());
			return ps.executeUpdate() == 1;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public User getUserByUsername(String username) throws NonexistingUserException {
		User u = null;
		String command = "SELECT * FROM ers_users WHERE ers_username = ?";
		try {
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				int id = rs.getInt(1);
				String password = rs.getString(3);
				String firstName = rs.getString(4);
				String lastName = rs.getString(5);
				String email = rs.getString(6);
				UserRole role = new UserRole(rs.getString(7));
				u = new User(id, new UserUPR(username, password, role), new UserPersInfo(firstName, lastName, email));
			} else {
				throw new NonexistingUserException();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return u;
	}

	@Override
	public User getUserByID(int id) throws NonexistingUserException {
		User u = null;
		String command = "SELECT * FROM ers_users WHERE ers_user_id = ?";
		try {
			ps = DB.getInstance().conn.prepareStatement(command);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String username = rs.getString(2);
				String password = rs.getString(3);
				String firstName = rs.getString(4);
				String lastName = rs.getString(5);
				String email = rs.getString(6);
				UserRole role = new UserRole(rs.getString(7));
				u = new User(id, new UserUPR(username, password, role), new UserPersInfo(firstName, lastName, email));
			} else {
				throw new NonexistingUserException();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return u;
	}

}
