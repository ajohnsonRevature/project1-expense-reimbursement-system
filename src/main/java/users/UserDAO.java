package users;

import java.util.List;

import exceptions.NonexistingUserException;

public interface UserDAO {
	public List<User> getAllUsersByRole(UserRole role);
	public boolean insertUser(User userToInsert);
	public boolean updateUser(User userToUpdate);
	public User getUserByUsername(String username) throws NonexistingUserException;
	public User getUserByID(int id) throws NonexistingUserException ;
}
