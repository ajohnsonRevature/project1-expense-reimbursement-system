package com.servlet.project1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.ObjectMapper;

import exceptions.NonexistingUserException;
import reimbursements.Reimbursement;
import reimbursements.ReimbursementOracleDAO;
import users.User;
import users.UserOracleDAO;

/**
 * Servlet implementation class ManHomeGetRows
 */
public class ManHomeGetRows extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManHomeGetRows() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String emp = request.getReader().readLine();
		List<Reimbursement> reimbs = null;
		if(emp.equals("all")) {
			reimbs = ReimbursementOracleDAO.getInstance()
					.getAllReimbursements();
		} else {
			try {
				reimbs = ReimbursementOracleDAO.getInstance()
						.getAllReimbursementsByUser(UserOracleDAO.getInstance().getUserByID(Integer.parseInt(emp)));
			} catch (NumberFormatException|NonexistingUserException e) {
				e.printStackTrace();
			}
		}
		ObjectMapper om = new ObjectMapper();
		om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
		try {
			String json = om.writeValueAsString(reimbs);
			out.append(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
