package com.servlet.project1;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import exceptions.NonexistingUserException;
import reimbursements.Reimbursement;
import reimbursements.ReimbursementOracleDAO;
import users.User;
import users.UserLoginAttempt;
import users.UserOracleDAO;

/**
 * Servlet implementation class login
 */
@WebServlet(asyncSupported = true, urlPatterns = { "/Login" })
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		ObjectMapper ob = new ObjectMapper();
//		response.setContentType("text/html");

		try {
			UserLoginAttempt ula = ob.readValue(request.getReader(), UserLoginAttempt.class);
			String username = ula.getUsername();
			String password = ula.getPassword();

			String destPage = "login.html";

			if (User.validateUnPw(username, password)) {
//				response.getWriter().append("CORRECT :)");
				HttpSession session = request.getSession();
				User u = UserOracleDAO.getInstance().getUserByUsername(username);
				session.setAttribute("loggedInUser", u);
				if (u.getUPR().getRole().toString().equalsIgnoreCase("EMPLOYEE")) {
					destPage = "emphome.html";
				} else if (u.getUPR().getRole().toString().equalsIgnoreCase("MANAGER")) {
					destPage = "manhome.html";
				}
			} 
//				else {
//				response.getWriter().append("Incorrect password. :(");
//			}
			response.getWriter().append(destPage);
		} catch (InvalidFormatException|NonexistingUserException e) {
			response.getWriter().append("login.html");
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		doGet(request, response);
	}
}
