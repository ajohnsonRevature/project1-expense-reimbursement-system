package com.servlet.project1;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Scanner;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import reimbursements.*;
import users.User;
import users.UserRole;

/**
 * Servlet implementation class Request
 */
public class Request extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Request() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		ObjectMapper ob = new ObjectMapper();
		response.setContentType("text/plain");
		try {
			Reimbursement r = ob.readValue(request.getReader(), Reimbursement.class);
			r.setUserID(((User)request.getSession().getAttribute("loggedInUser")).getId());
			if (ReimbursementOracleDAO.getInstance().insertReimbursement(r)) {
				response.getWriter().append("Request submitted successfully. :)");
			} else {
				response.getWriter().append("Request failed to submit. :(");
			}
		} catch (InvalidFormatException e) {
			response.getWriter().append("Unable to parse request! D:");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
